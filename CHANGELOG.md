# Changelog
## Version 1.0.1864.643521
This version is included in SurgeOS 15 Beta 1. It is the initial release of Surge Keyboard, which, in jpbROM, was the unmodified AOSP Keyboard.